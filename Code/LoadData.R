


load_data  <- function(location) {
  data_df <- read.csv(location)
  data_df[,'Date']<- as.Date(as.character(data_df[,'Date']))
  data_df[,'Ticker']<- as.character(data_df[,'Ticker'])
  data_df[,'Time.To.Default'] <- factor(as.character(data_df[,'Time.To.Default']),levels = c('1 MONTH','2 to 6 MONTHS','7 to 24 MONTHS','>2 YEARS'),ordered = TRUE)
  return(data_df)
}